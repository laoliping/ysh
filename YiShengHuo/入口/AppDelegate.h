//
//  AppDelegate.h
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

