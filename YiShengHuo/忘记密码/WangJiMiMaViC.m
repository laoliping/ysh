//
//  WangJiMiMaViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "WangJiMiMaViC.h"

@interface WangJiMiMaViC ()<UITextFieldDelegate>

@property(nonatomic,strong)UITextField  *accountTF;
@property(nonatomic,strong)UITextField *pwdTF;
@property(nonatomic,strong) UIButton *verifyCodeBtn;
@property(nonatomic,strong)UITextField  *verifyCodeTF;


@end

@implementation WangJiMiMaViC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"忘记密码";
    self.view.backgroundColor = [UIColor whiteColor];
    _accountTF = [[UITextField alloc] initWithFrame:CGRectMake(47, 195, SCREEN_WIDTH- 47*2-82, 42)];
    _accountTF.borderStyle = UITextBorderStyleRoundedRect;
    _accountTF.placeholder = @"请输入手机号";
    _accountTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    _accountTF .delegate = self;
    _accountTF.tag = 199;
    UIImageView *leftIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,20, 20)];
    leftIcon.image = [UIImage imageNamed:@"login_icon_user_n"];
    leftIcon.contentMode = UIViewContentModeCenter;
    _accountTF.leftView = leftIcon;
    _accountTF .leftViewMode = UITextFieldViewModeAlways;
    _accountTF .keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:_accountTF];
    
    _verifyCodeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _verifyCodeBtn.frame = CGRectMake(47+SCREEN_WIDTH- 47*2-85,195,85,42);
    _verifyCodeBtn.backgroundColor = MainColor;
    [_verifyCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    _verifyCodeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [_verifyCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_verifyCodeBtn addTarget:self action:@selector(codeSendCode:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_verifyCodeBtn];
    
    _verifyCodeTF = [[UITextField alloc] initWithFrame:CGRectMake(47, 267, SCREEN_WIDTH- 47*2, 42)];
    _verifyCodeTF.borderStyle = UITextBorderStyleRoundedRect;
    _verifyCodeTF.placeholder = @"请输入验证码";
    _verifyCodeTF.clearsOnBeginEditing = YES;
    _verifyCodeTF.delegate = self;
    _verifyCodeTF.tag =101;
    UIImageView *codeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    codeIcon.image = [UIImage imageNamed:@"login_icon_lock_p"];
    codeIcon.contentMode = UIViewContentModeCenter;
    _verifyCodeTF.leftView = codeIcon;
    _verifyCodeTF.leftViewMode = UITextFieldViewModeAlways;
    _verifyCodeTF .keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:_verifyCodeTF];

        
    _pwdTF = [[UITextField alloc] initWithFrame:CGRectMake(47, 339, SCREEN_WIDTH- 47*2, 42)];
    _pwdTF.borderStyle = UITextBorderStyleRoundedRect;
    _pwdTF.placeholder = @"请输入密码";
    _pwdTF.clearsOnBeginEditing = YES;
    _pwdTF.delegate = self;
    _pwdTF.tag =101;
    UIImageView *PSWIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    PSWIcon.image = [UIImage imageNamed:@"login_icon_lock_p"];
    PSWIcon.contentMode = UIViewContentModeCenter;
    _pwdTF.leftView = PSWIcon;
    _pwdTF.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:_pwdTF];

    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    loginBtn.frame = CGRectMake(47 , 411, SCREEN_WIDTH-94, 48);
    [loginBtn setTitle:@"确定" forState:UIControlStateNormal];
    loginBtn.tintColor = [UIColor whiteColor];
    [loginBtn addTarget:self action:@selector(sureBtnCheck:) forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = MainColor;
    [loginBtn.layer setMasksToBounds:YES];
    [loginBtn.layer setCornerRadius:6.0];
    [self.view addSubview:loginBtn];
    
}
-(void)codeSendCode:(UIButton *)sender {

    [self textFieldHide];
    if (_accountTF.text.length != 11) {

        [JRToast showWithText:@"请输入11位有效的手机号码" bottomOffset:100.0f duration:3.0f];
        return;
    }

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText=@"正在发送";
    [self.view addSubview:hud];
    
    [[WangZhangNetWork sharedNetManager]VerifyTheMobilePhoneNumberByPhone:_accountTF.text AndAction:@"02" withCompletionBlock:^(id responseObj, NSError *error) {
        
        NSLog(@"%@== %@",responseObj,error);
        
        
        if ([responseObj[@"code"] integerValue] == 0) {
            hud.labelText=@"发送成功";
            [hud hide:YES afterDelay:1.0];
            
            [self->_verifyCodeBtn countDownFromTime:60 title:@"获取验证码" unitTitle:@"秒" mainColor:[UIColor whiteColor] countColor:[UIColor blueColor]];
        }else{
            hud.labelText=[NSString stringWithFormat:@"%@",responseObj[@"msg"]];
            [hud hide:YES afterDelay:1.0];

            return ;
        }
    }];

    
}
-(void)sureBtnCheck:(UIButton *)sender {

    [self textFieldHide];
    
    if (_accountTF.text.length != 11) {
        [JRToast showWithText:@"请输入11位有效的手机的号码" bottomOffset:SCREEN_HEIGHT-100 duration:3.0f];
        return;
    }
    if (!_verifyCodeTF.text||[_verifyCodeTF.text isEqualToString:@""]) {
        [JRToast showWithText:@"请输入验证码" bottomOffset:SCREEN_HEIGHT-100 duration:3.0f];
        return;
    }
    if (_pwdTF.text.length < 6 ||_pwdTF.text.length > 16) {
        [JRToast showWithText:@"请输入密码" bottomOffset:100.f duration:3.0f];
        return;
    }

    MBProgressHUD*hud=[[MBProgressHUD alloc]initWithView:self.view];
    hud.labelText=@"正在设置";
    [self.view addSubview:hud];
    [hud show:YES];

    NSDictionary *param = @{@"mobile":_accountTF.text,@"password":_pwdTF.text,@"validateCode":_verifyCodeTF.text};
    
    [[WangZhangNetWork sharedNetManager]resetPwdJsonByByParams:param withcompletionblock:^(id responseObj, NSError *error) {
        NSLog(@"%@== %@",responseObj,error);
        
        if ([responseObj[@"code"] integerValue] == 0) {
            hud.labelText=@"成功";
            [hud hide:YES afterDelay:1.0];
            [self.navigationController popViewControllerAnimated:NO];
        }else{
            hud.labelText=[NSString stringWithFormat:@"%@",responseObj[@"msg"]];
            [hud hide:YES afterDelay:1.0];

            return ;
        }
    }];
    
    
}
-(void)textFieldHide{
    [self.view endEditing:YES];
}

@end
