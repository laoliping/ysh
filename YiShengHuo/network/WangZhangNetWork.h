//
//  WangZhangNetWork.h
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^WangZhangNetwork)(id responseObj,NSError *error);

@interface WangZhangNetWork : NSObject

+(instancetype)sharedNetManager;

//验证码1
-(void)VerifyTheMobilePhoneNumberByPhone:(NSString*)phoneNum AndAction:(NSString*)action withCompletionBlock:(WangZhangNetwork)block;
//用户注册2
-(void)RegisterByMobileByByParams:(id)params withcompletionblock: (WangZhangNetwork)block;
//忘记密码3
-(void)resetPwdJsonByByParams:(id)params withcompletionblock: (WangZhangNetwork)block;
//登录4
-(void)UserInfoLoginByParams:(id)params withConpletionBlock:(WangZhangNetwork)block;
//用户查询5
-(void)FindCurrentLoginUserInfoByParams:(id)params withConpletionBlock:(WangZhangNetwork)block;
//修改登录密码6
-(void)ModifyPwdByParams:(id)params withConpletionBlock:(WangZhangNetwork)block;
//退出登录7
-(void)loginOutByParams:(NSString *)params withConpletionBlock:(WangZhangNetwork)block;
//查询家庭8
-(void)FindFamilyByParams:(NSString *)params withConpletionBlock:(WangZhangNetwork)block;
//查询家庭房间9
-(void)FindFamilyRoomByHomeId:(NSString *)homeId withConpletionBlock:(WangZhangNetwork)block;
//查询设备列表10
-(void)FindHomeDeviceByParams:(id)params andPageNum:(NSString *)pageNem andPagesize:(NSString *)pagesize withConpletionBlock:(WangZhangNetwork)block;
//场景11
-(void)getSceneFindByHomeId:(NSString *)homeId AndSceneType:(NSString*)sceneType withConpletionBlock:(WangZhangNetwork)block;
// 执行手动场景12
-(void)zhixingSceneStartBySceneId:(NSString *)sceneId withConpletionBlock:(WangZhangNetwork)block;
//控制设备
-(void)HomeDeviceControlByParams:(id)params withConpletionBlock:(WangZhangNetwork)block;
//消息中心
-(void)GetMsgfindByParams:(id)params AndPageNum:(NSString *)pageNem withConpletionBlock:(WangZhangNetwork)block;


@end

