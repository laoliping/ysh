//
//  WangZhangNetWork.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "WangZhangNetWork.h"

@implementation WangZhangNetWork


+(instancetype)sharedNetManager{
    
    static WangZhangNetWork *network = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        if (network == nil) {
            network = [[WangZhangNetWork alloc]init];
        }
    });

    return network;
}
-(NSString *)getToken
{
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    return [userdefaults objectForKey:@"access_token"];
}
//验证码1
-(void)VerifyTheMobilePhoneNumberByPhone:(NSString*)phoneNum AndAction:(NSString*)action withCompletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    NSString *SMSURL = [NSString stringWithFormat:@"%@%@/security/sendValidateCode?mobile=%@&action=%@&version=%@",Public_URL,User_URL,phoneNum,action,@"V1.7.0"];
    
    
    [manager GET:SMSURL parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         block(responseObject,nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        block(nil,error);
    }];

}

//用户注册2
-(void)RegisterByMobileByByParams:(id)params withcompletionblock: (WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    NSString *url = [NSString stringWithFormat:@"%@%@/security/register",Public_URL,User_URL];
    
    [manager POST:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         block(responseObject,nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         block(nil,error);
    }];
    
}
//忘记密码3
-(void)resetPwdJsonByByParams:(id)params withcompletionblock: (WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    NSString *url = [NSString stringWithFormat:@"%@%@/security/forgot",Public_URL,User_URL];
    
   [manager POST:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           block(responseObject,nil);
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           block(nil,error);
      }];
}
//登录4
-(void)UserInfoLoginByParams:(id)params withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer.timeoutInterval = 20.f;
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    NSString *url = [NSString stringWithFormat:@"%@%@/security/login",Public_URL,User_URL];
    
    [manager POST:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         block(responseObject,nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         block(nil,error);
    }];
}

//用户查询5
-(void)FindCurrentLoginUserInfoByParams:(id)params withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/user/findCurrentLoginUserInfo",Public_URL,User_URL];
    
    [manager GET:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         block(responseObject,nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        block(nil,error);
    }];
    
}

//修改登录密码6
-(void)ModifyPwdByParams:(id)params withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/user/modifyPassword",Public_URL,User_URL];
    
    [manager POST:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            block(responseObject,nil);
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            block(nil,error);
    }];
    
}
//退出登录7
-(void)loginOutByParams:(NSString *)params withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/user/logout",Public_URL,User_URL];
    
     [manager POST:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            block(responseObject,nil);
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            block(nil,error);
    }];
}

//查询家庭8
-(void)FindFamilyByParams:(NSString *)params withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@/Home/find/1/10000",Public_URL,Device_URL];
    
    [manager GET:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         block(responseObject,nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        block(nil,error);
    }];
}

//查询家庭房间9
-(void)FindFamilyRoomByHomeId:(NSString *)homeId withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/Home/find/%@/room/1/10000",Public_URL,Device_URL,homeId];
    
    [manager GET:url parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         block(responseObject,nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        block(nil,error);
    }];
}

//查询设备列表10
-(void)FindHomeDeviceByParams:(id)params andPageNum:(NSString *)pageNem andPagesize:(NSString *)pagesize withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
    [manager.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"from"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/HomeDevice/find/%@/%@",Public_URL,Device_URL,pageNem,pagesize];
    
     [manager GET:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         block(responseObject,nil);
         
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        block(nil,error);
        
        
    }];
}

//场景11
-(void)getSceneFindByHomeId:(NSString *)homeId AndSceneType:(NSString*)sceneType withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/Scene/find/%@/%@/1/10000",Public_URL,Device_URL,homeId,sceneType];
    
    [manager GET:url parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         block(responseObject,nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        block(nil,error);
    }];
}

// 执行手动场景12
-(void)zhixingSceneStartBySceneId:(NSString *)sceneId withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/Scene/start/%@",Public_URL,Device_URL,sceneId];
    
    [manager POST:url parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            block(responseObject,nil);
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            block(nil,error);
    }];
}
//控制设备
-(void)HomeDeviceControlByParams:(id)params withConpletionBlock:(WangZhangNetwork)block
{
    AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
    [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
    [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
     [manager.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"from"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/HomeDevice/control",Public_URL,Device_URL];
    
    [manager POST:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             block(responseObject,nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             block(nil,error);
     }];
    
}
//消息中心
-(void)GetMsgfindByParams:(id)params AndPageNum:(NSString *)pageNem withConpletionBlock:(WangZhangNetwork)block
{
   AFHTTPSessionManager*manager= [AFHTTPSessionManager manager];
   manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
   manager.requestSerializer = [AFHTTPRequestSerializer serializer];
   manager.responseSerializer = [AFJSONResponseSerializer serializer];
   [manager.requestSerializer setValue:@"Set-Cookie" forHTTPHeaderField:@"cookie"];
   [manager.requestSerializer setValue:APPID forHTTPHeaderField:@"appId"];
   [manager.requestSerializer setValue:[self getToken] forHTTPHeaderField:@"access_token"];
    [manager.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"from"];
   
   NSString *url = [NSString stringWithFormat:@"%@%@/Msg/find/%@/%@",Public_URL,Device_URL,pageNem,@"100"];
   
   [manager GET:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           block(responseObject,nil);
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          block(nil,error);
      }];
    
}

@end
