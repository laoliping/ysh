//
//  ShebeiViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "ShebeiViC.h"

@interface ShebeiViC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *shebeiTabView;
@property (nonatomic,strong)YoShengHuoHomeDataInfo *curFamily;
@property (nonatomic,strong)NSMutableArray *shebeiData;

@end

@implementation ShebeiViC
-(instancetype)init
{
    if (self=[super init]) {
        self.shebeiData = [[NSMutableArray alloc]init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.title = @"设备";
    
   [self getSimpleDeviceList];
    
    self.shebeiTabView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) style:UITableViewStylePlain];
    self.shebeiTabView.dataSource = self;
    self.shebeiTabView.delegate = self;
    [self.shebeiTabView registerClass:[SheBeiCell class] forCellReuseIdentifier:@"SheBeiCell"];
    [self.view addSubview:self.shebeiTabView];
    
    self.shebeiTabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.shebeiTabView.mj_header endRefreshing];
        [self getSimpleDeviceList];
        
        [self.shebeiTabView.mj_footer resetNoMoreData];
    }];
    
}

-(void)getSimpleDeviceList {
    
    self.curFamily = [YoShengHuoHomeData shareInstance].data[0];
    
    NSLog(@"%@",self.curFamily);
    
    NSDictionary *param = @{@"homeId":self.curFamily.homeId};

    
    [[WangZhangNetWork sharedNetManager]FindHomeDeviceByParams:param andPageNum:@"1" andPagesize:@"1000" withConpletionBlock:^(id responseObject, NSError *error) {
        
        NSLog(@"%@",responseObject);
        
        [self.shebeiData removeAllObjects];
        
        DEviceModel *list = [DEviceModel yy_modelWithJSON:responseObject];
        NSLog(@"%@",list);
        
        [self.shebeiData addObjectsFromArray:list.data];
        
        [self.shebeiTabView reloadData];
        
        
        
    }];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shebeiData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SheBeiCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SheBeiCell" forIndexPath:indexPath];
    
    DEviceModelItem *model = [self.shebeiData objectAtIndex:indexPath.row];
    
   [cell.deviceIMG sd_setImageWithURL:[NSURL URLWithString:model.iconPath] completed:nil];
    cell.deviceName.text = model.deviceNick;
    cell.onlineTitle.text = model.deviceStatus;
    
    if ([model.deviceType isEqualToString:@"ON_OFF_SWITCH"]&& model.deviceEndpoints.count==1) {
           cell.deviceSwitch.hidden = NO;
        
       }else if ([model.deviceType isEqualToString:@"THERMOSTAT"]) {
           cell.deviceSwitch.hidden = NO;
          
       }else if ([model.deviceType isEqualToString:@"SMART_PLUG"]&& model.deviceEndpoints.count==1) {
           cell.deviceSwitch.hidden = NO;
           
       }else if ([model.deviceType isEqualToString:@"MAINS_POWER_OUTLET"]&& model.deviceEndpoints.count==1) {
           cell.deviceSwitch.hidden = NO;
           
       }else if ([model.deviceType isEqualToString:@"RGB_LAMP"]&& model.deviceEndpoints.count==1) {
           cell.deviceSwitch.hidden = NO;
         
       }else if ([model.deviceType isEqualToString:@"COLOR_TEMPERATURE_LIGHT"]&& model.deviceEndpoints.count==1) {
           cell.deviceSwitch.hidden = NO;
           
       }else if ([model.deviceType isEqualToString:@"COLOR_CONTROLLER"]&& model.deviceEndpoints.count==1) {
           cell.deviceSwitch.hidden = NO;
           
       }else if ([model.deviceType isEqualToString:@"DIMMER_SWITCH"]&& model.deviceEndpoints.count==1) {
           cell.deviceSwitch.hidden = NO;
          
       }else if ([model.deviceType isEqualToString:@"ON_OFF_SWITCH"]&& model.deviceEndpoints.count==2) {
           cell.deviceSwitch.hidden = NO;
           
       }else if ([model.deviceType isEqualToString:@"ON_OFF_SWITCH"]&& model.deviceEndpoints.count==3) {
           cell.deviceSwitch.hidden = NO;
           
       }else if ([model.deviceType isEqualToString:@"ON_OFF_SWITCH"]&& model.deviceEndpoints.count==4) {
           cell.deviceSwitch.hidden = NO;
           
       }else if ([model.deviceType isEqualToString:@"WINDOW_COVERING"]&& model.deviceEndpoints.count==1) {
           cell.deviceSwitch.hidden = NO;
           
       }else if ([model.deviceType isEqualToString:@"WINDOW_COVERING"]&& model.deviceEndpoints.count==2) {
           cell.deviceSwitch.hidden = NO;
          
       }else if ([model.deviceType isEqualToString:@"CURTAIN_MOTOR"]) {
           cell.deviceSwitch.hidden = NO;
           
       }else{
           cell.deviceSwitch.hidden = YES;
          
       }
       
       if (cell.deviceSwitch.hidden == NO) {
           
           NSDictionary *devicedic = model.deviceEndpoints[0];
           NSArray *productArr = devicedic[@"productFunctions"];
           NSString *ONString = productArr[0][@"value"];


           if ([ONString isEqualToString:@"01"]) {
               
               cell.deviceSwitch.on = YES;
               
           }else{
               cell.deviceSwitch.on = NO;
           }
       }
    
    [cell.deviceSwitch addTarget:self action:@selector(deviceSwitchPress:) forControlEvents:UIControlEventValueChanged];
    cell.deviceSwitch.tag = indexPath.row + 888;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
-(void)deviceSwitchPress:(UISwitch *)sender
{
    DEviceModelItem *model = [self.shebeiData objectAtIndex:sender.tag - 888];
    sender.selected = !sender.selected;
    NSString *kaiguanValue;
    if (sender.on) {
        kaiguanValue =@"01";
    }else{
        kaiguanValue =@"00";
    }
    NSLog(@"%@",kaiguanValue);
    
    
    NSDictionary *param = @{@"homeDeviceId":model.homeDeviceId,@"value":kaiguanValue,@"property":@"switch_status",@"endpoint":@"-1"};
    NSLog(@"%@",param);
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.view addSubview:hud];
    
    [[WangZhangNetWork sharedNetManager]HomeDeviceControlByParams:param withConpletionBlock:^(id responseObj, NSError *error) {
        
       if ([responseObj[@"code"] integerValue] == 0) {

           hud.labelText = @"OK";
           [hud hide:YES afterDelay:1.0];
        }else{
        hud.labelText = @"失败";
           [hud hide:YES afterDelay:1.0];
           return ;
        }
        
    }];
    
}


@end
