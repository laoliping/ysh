//
//  SheBeiCell.h
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SheBeiCell : UITableViewCell

@property (nonatomic, strong) UIImageView *deviceIMG;
@property (nonatomic, strong) UILabel *deviceName;
@property (nonatomic, strong) UILabel *onlineTitle;
@property (nonatomic, strong) UISwitch *deviceSwitch;

@end

NS_ASSUME_NONNULL_END
