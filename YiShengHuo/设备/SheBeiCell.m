//
//  SheBeiCell.m
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "SheBeiCell.h"

@implementation SheBeiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        UIView *kaiview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 89)];
        kaiview.backgroundColor = [UIColor whiteColor];
        kaiview.layer.cornerRadius =10;
        kaiview.layer.masksToBounds = YES;
        [self.contentView addSubview:kaiview];
        
        self.deviceIMG = [[UIImageView alloc] initWithFrame:CGRectMake(35, (90-48)/2, 48, 48)];
        [self.contentView addSubview:self.deviceIMG];
        
        self.deviceName = [[UILabel alloc] initWithFrame:CGRectMake(102, 15,[[UIScreen mainScreen] bounds].size.width-102, 30)];
       self.deviceName.font = [UIFont systemFontOfSize:14];
       self.deviceName.text = @"名字";
       self.deviceName.textColor = MainColor;
       self.deviceName.textAlignment = NSTextAlignmentLeft;
       [self.contentView addSubview:self.deviceName];
        
       self.onlineTitle = [[UILabel alloc] initWithFrame:CGRectMake(102, 45, [[UIScreen mainScreen] bounds].size.width-102, 30)];
       self.onlineTitle.font = [UIFont systemFontOfSize:12];
        self.onlineTitle.text = @"在线or离线";
        self.onlineTitle.textColor = MainColor;
        self.onlineTitle.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.onlineTitle];
        
        self.deviceSwitch = [[UISwitch alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-49-20, (90-31)/2, 49, 31)];
        self.deviceSwitch.onTintColor = MainColor;
        self.deviceSwitch.transform = CGAffineTransformMakeScale(1.0, 1.0);
        [self.contentView addSubview:self.deviceSwitch];
        

    }
    return self;
}

@end
