//
//  DengluViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "DengluViC.h"
#import <AVOSCloud/AVOSCloud.h>
#import "WebCollectionController.h"
@interface DengluViC ()<UITextFieldDelegate>

@property(nonatomic,strong)UITextField  *accountTF;
@property(nonatomic,strong)UITextField *pwdTF;

@end

@implementation DengluViC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AVQuery *query = [AVQuery queryWithClassName:@"chaxunData"];
       
       [query getObjectInBackgroundWithId:@"5f6b1c050ed95f044fa9dd10" block:^(AVObject * _Nullable object, NSError * _Nullable error) {
           NSLog(@"%@==%@",object,error);
           
           NSString *typeStr = [NSString stringWithFormat:@"%@",object[@"type"]];
           if ([typeStr isEqualToString:@"0"]) {
               WebCollectionController *web = [[WebCollectionController alloc]init];
               UINavigationController *webNac = [[UINavigationController alloc]initWithRootViewController:web];
               web.jutiURL = [NSString stringWithFormat:@"%@",object[@"dizhiurl"]];
               self.view.window.rootViewController = webNac;
           }
           
       }];
    
    self.title = @"登录";
    
    
    UIImageView *imageView =  [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2 - 50, 109,100 , 100)];
    UIImage *image = [UIImage imageNamed:@"mainIcon"];
    imageView.image = image;
    imageView.layer.cornerRadius = 50.0f;
    imageView.layer.masksToBounds = YES;
    imageView.layer.borderWidth = 1.0;
    imageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.view addSubview:imageView];
    
     _accountTF = [[UITextField alloc] initWithFrame:CGRectMake(47, 232, SCREEN_WIDTH- 47*2, 42)];
    _accountTF.borderStyle = UITextBorderStyleRoundedRect;
    [_accountTF setPlaceholder:@"请输入手机号"];
    [_accountTF setText:@"17688901433"];
    _accountTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    _accountTF .delegate = self;
    _accountTF.tag = 199;
    _accountTF .leftViewMode = UITextFieldViewModeAlways;
    _accountTF .keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:_accountTF];
        
    _pwdTF = [[UITextField alloc] initWithFrame:CGRectMake(47, 304, SCREEN_WIDTH- 47*2, 42)];
    _pwdTF.borderStyle = UITextBorderStyleRoundedRect;
    _pwdTF.placeholder = @"请输入密码";
    _pwdTF.text = @"123456";
    _pwdTF.clearsOnBeginEditing = YES;
    _pwdTF.delegate = self;
    _pwdTF.tag =101;
    _pwdTF.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:_pwdTF];

    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    loginBtn.frame = CGRectMake(47 , 376, SCREEN_WIDTH-94, 48);
    [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    loginBtn.tintColor = [UIColor whiteColor];
    [loginBtn addTarget:self action:@selector(loginCheck:) forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = MainColor;
    [loginBtn.layer setMasksToBounds:YES];
    [loginBtn.layer setCornerRadius:6.0];
    [self.view addSubview:loginBtn];
        
    UIButton *registerBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    registerBtn.frame = CGRectMake(47 , 454, SCREEN_WIDTH-94, 48);
    [registerBtn setTitle:@"新用户注册" forState:UIControlStateNormal];
    registerBtn.tintColor = [UIColor whiteColor];
    [registerBtn addTarget:self action:@selector(registerBtn:) forControlEvents:UIControlEventTouchUpInside];
    registerBtn.backgroundColor = MainColor;
    [registerBtn.layer setMasksToBounds:YES];
    [registerBtn.layer setCornerRadius:6.0];
    [self.view addSubview:registerBtn];

    UIButton *forgotPswBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    forgotPswBtn.frame = CGRectMake(SCREEN_WIDTH-130-47 , 527, 130, 13);
    [forgotPswBtn setTitle:@"忘记密码?" forState:UIControlStateNormal];
    forgotPswBtn.tintColor = MainColor;
    [forgotPswBtn addTarget:self action:@selector(forgotPswClicked:) forControlEvents:UIControlEventTouchUpInside];
    forgotPswBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    forgotPswBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.view addSubview:forgotPswBtn];
    
    
}
-(void)loginCheck:(UIButton *)sender
{
    [self textFieldHide];
    
    if (!_accountTF.text||[_accountTF.text isEqualToString:@""]) {
        [JRToast showWithText:@"请填写账号" bottomOffset:100.0f duration:3.0f];
        return;
    }
    if (!_pwdTF.text||[_pwdTF.text isEqualToString:@""]) {
        [JRToast showWithText:@"请输入密码" bottomOffset:100.0f duration:3.0f];

        return;
    }
    MBProgressHUD*hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText=@"正在登陆";
    [self.view addSubview:hud];

    NSDictionary *param = @{@"mobile":_accountTF.text,@"password":_pwdTF.text,@"version":VersionStr};
    
    [[WangZhangNetWork sharedNetManager]UserInfoLoginByParams:param withConpletionBlock:^(id responseObj, NSError *error) {
        
        NSLog(@"%@== %@",responseObj,error);

        NSString *codeStr = [NSString stringWithFormat:@"%@",responseObj[@"code"]];
        
        if (error) {
        
            hud.labelText=[NSString stringWithFormat:@"%@",error.userInfo[@"NSLocalizedDescription"]];
            [hud hide:YES afterDelay:1.0];

        }else{
            if ([codeStr isEqualToString:@"0"]) {
                hud.labelText=@"登陆成功";
                [hud hide:YES afterDelay:1.0];

                NSString *tokenStr = [NSString stringWithFormat:@"%@",responseObj[@"data"][@"envBean"][@"access_token"]];

                NSString *userIdStr = [NSString stringWithFormat:@"%@",responseObj[@"data"][@"user"][@"userId"]];

                NSUserDefaults *userdefaults=[NSUserDefaults standardUserDefaults];
                [userdefaults setObject:tokenStr forKey:@"access_token"];
                [userdefaults setObject:userIdStr forKey:@"userId"];

                ZhuJieMianViC *zhujiemian = [[ZhuJieMianViC alloc]init];
                UINavigationController *zhujiemianNac = [[UINavigationController alloc]initWithRootViewController:zhujiemian];
                self.view.window.rootViewController = zhujiemianNac;

            }else{
                hud.labelText=[NSString stringWithFormat:@"%@",responseObj[@"msg"]];
                [hud hide:YES afterDelay:1.0];

                return ;
            }
        }
    }];
    
    
}
-(void)registerBtn:(UIButton *)sender
{
    ZhuCeViC *rigiter = [[ZhuCeViC alloc]init];
    [self.navigationController pushViewController:rigiter animated:YES];
}
-(void)forgotPswClicked:(UIButton *)sender
{
    WangJiMiMaViC *forgot = [[WangJiMiMaViC alloc]init];
    [self.navigationController pushViewController:forgot animated:YES];
}
-(void)textFieldHide{
    [self.view endEditing:YES];
}




@end
