//
//  ZhuCeViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "ZhuCeViC.h"

@interface ZhuCeViC ()<UITextFieldDelegate>

@property(nonatomic,strong)UITextField  *accountTF;
@property(nonatomic,strong)UITextField *pwdTF;
@property(nonatomic,strong) UIButton *verifyCodeBtn;
@property(nonatomic,strong)UITextField  *verifyCodeTF;

@end

@implementation ZhuCeViC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"注册";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _accountTF = [[UITextField alloc] initWithFrame:CGRectMake(47, 195, SCREEN_WIDTH- 47*2-82, 42)];
    _accountTF.borderStyle = UITextBorderStyleRoundedRect;
    _accountTF.placeholder = @"请输入手机号";
    _accountTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    _accountTF .delegate = self;
    _accountTF .leftViewMode = UITextFieldViewModeAlways;
    _accountTF .keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:_accountTF];
        
    _verifyCodeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _verifyCodeBtn.frame = CGRectMake(47+SCREEN_WIDTH- 47*2-85,195,85,42);
    _verifyCodeBtn.backgroundColor = MainColor;
    [_verifyCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    _verifyCodeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [_verifyCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_verifyCodeBtn addTarget:self action:@selector(codeSendCode:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_verifyCodeBtn];
    
    _verifyCodeTF = [[UITextField alloc] initWithFrame:CGRectMake(47, 267, SCREEN_WIDTH- 47*2, 42)];
    _verifyCodeTF.borderStyle = UITextBorderStyleRoundedRect;
    _verifyCodeTF.placeholder = @"请输入验证码";
    _verifyCodeTF.clearsOnBeginEditing = YES;
    _verifyCodeTF.delegate = self;
    _verifyCodeTF.tag =101;
    _verifyCodeTF.leftViewMode = UITextFieldViewModeAlways;
    _verifyCodeTF .keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:_verifyCodeTF];

        
    _pwdTF = [[UITextField alloc] initWithFrame:CGRectMake(47, 339, SCREEN_WIDTH- 47*2, 42)];
    _pwdTF.borderStyle = UITextBorderStyleRoundedRect;
    _pwdTF.placeholder = @"请输入密码";
    _pwdTF.clearsOnBeginEditing = YES;
    _pwdTF.delegate = self;
    _pwdTF.tag =101;
    _pwdTF.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:_pwdTF];


    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    loginBtn.frame = CGRectMake(47 , 411, SCREEN_WIDTH-94, 48);
    [loginBtn setTitle:@"注册" forState:UIControlStateNormal];
    loginBtn.tintColor = [UIColor whiteColor];
    [loginBtn addTarget:self action:@selector(rigisterBtnCheck:) forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = MainColor;
    [loginBtn.layer setMasksToBounds:YES];
    [loginBtn.layer setCornerRadius:6.0];
    [self.view addSubview:loginBtn];
    
}
-(void)codeSendCode:(UIButton *)sender {

    [self textFieldHide];
  
    if (_accountTF.text.length != 11) {
        [JRToast showWithText:@"请输入11位有效的手机号码" bottomOffset:100.0f duration:3.0f];
        return;
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText=@"正在发送";
    [self.view addSubview:hud];
    
    [[WangZhangNetWork sharedNetManager]VerifyTheMobilePhoneNumberByPhone:_accountTF.text AndAction:@"01" withCompletionBlock:^(id responseObj, NSError *error) {
        
        NSLog(@"%@== %@",responseObj,error);
        
        
        if ([responseObj[@"code"] integerValue] == 0) {
            hud.labelText=@"发送成功";
            [hud hide:YES afterDelay:1.0];
            
            [self->_verifyCodeBtn countDownFromTime:60 title:@"获取验证码" unitTitle:@"秒" mainColor:[UIColor whiteColor] countColor:[UIColor blueColor]];
        }else{
            hud.labelText=[NSString stringWithFormat:@"%@",responseObj[@"msg"]];
            [hud hide:YES afterDelay:1.0];

            return ;
        }
    }];

}
-(void)rigisterBtnCheck:(UIButton *)sender {

    
    [self textFieldHide];
     
    if (_accountTF.text.length != 11) {
         [JRToast showWithText:@"请使用11位有效的手机的号码！" bottomOffset:100 duration:3.0f];

         return;
     }
     if (!_verifyCodeTF.text||[_verifyCodeTF.text isEqualToString:@""]) {
         [JRToast showWithText:@"请输入验证码" bottomOffset:100 duration:3.0f];
         return;
     }

     if (_pwdTF.text.length < 6 ||_pwdTF.text.length > 16) {
         [JRToast showWithText:@"请输入密码" bottomOffset:100.f duration:3.0f];
         return;
     }



     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     hud.labelText=@"正在注册";
     [self.view addSubview:hud];

     NSDictionary *param = @{@"mobile":_accountTF.text,@"password":_pwdTF.text,@"validateCode":_verifyCodeTF.text,@"accountType":@"4"};
    
    [[WangZhangNetWork sharedNetManager]RegisterByMobileByByParams:param withcompletionblock:^(id responseObj, NSError *error) {
        
        if ([responseObj[@"code"] integerValue] == 0) {
            hud.labelText=@"注册成功";
            [hud hide:YES afterDelay:1.0];
            [self.navigationController popViewControllerAnimated:NO];
        }else{
            hud.labelText=[NSString stringWithFormat:@"%@",responseObj[@"msg"]];
            [hud hide:YES afterDelay:1.0];

            return ;
        }
        
    }];
    
}
-(void)textFieldHide{
    [self.view endEditing:YES];
}

@end
