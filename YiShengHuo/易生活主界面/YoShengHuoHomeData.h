//
//  YoShengHuoHomeData.h
//  YiShengHuo
//
//  Created by honfan on 2020/4/30.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface YoShengHuoHomeData : NSObject

@property (nonatomic,strong)NSArray *data;

+ (YoShengHuoHomeData *)shareInstance;

+(void)setInstance:(YoShengHuoHomeData *)info;

@end

@interface YoShengHuoHomeDataInfo : NSObject

@property (nonatomic,strong)NSString *homeId;
@property (nonatomic,copy)NSString *homeName;
@property (nonatomic,copy)NSString *provinceCode;
@property (nonatomic,copy)NSString *cityCode;
@property (nonatomic,copy)NSString *countyCode;
@property (nonatomic,copy)NSString *adCode;
@property (nonatomic,copy)NSString *createBy;
@property (nonatomic,copy)NSString *createTime;
@property (nonatomic,copy)NSString *creatorId;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *updateBy;
@property (nonatomic,copy)NSString *updateTime;

@end


