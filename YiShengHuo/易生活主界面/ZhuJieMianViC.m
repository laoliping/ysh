//
//  ZhuJieMianViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "ZhuJieMianViC.h"

@interface ZhuJieMianViC ()

@property (nonatomic,strong)YoShengHuoHomeDataInfo *curFamily;

@end

@implementation ZhuJieMianViC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"易生活";
    
    
   
    NSArray *imageArr = @[@"设备",@"场景",@"管理"];
    NSArray *titleArr = @[@"设备",@"场景",@"管理"];
    NSArray *ColorArr = @[[UIColor colorWithHexString:@"#1F9CFB" alpha:1.0f],[UIColor colorWithHexString:@"#1DD8A5" alpha:1.0f],[UIColor colorWithHexString:@"#740000" alpha:1.0f]];

    
    CGFloat Hight;
    if (SCREEN_HEIGHT >= 812) {
        Hight =100;
    }else{
        Hight =80;
    }
    for (int i=0; i<imageArr.count; i++) {
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(20, Hight+110*i, SCREEN_WIDTH-40, 100)];
    topView.backgroundColor = ColorArr[i];
    topView.userInteractionEnabled = YES;
    topView.layer.cornerRadius = 10;
    [topView.layer setMasksToBounds:YES];
    [self.view addSubview:topView];
    
    UIButton *xinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    xinBtn.frame = CGRectMake(0, 0, SCREEN_WIDTH-40, 100);
    xinBtn.tag = 1+i;
    [xinBtn addTarget:self action:@selector(singleSelected:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:xinBtn];
    
    UIImageView *shaoView = [[UIImageView alloc] initWithFrame:CGRectMake(40, 27, 46, 46)];
    shaoView.image = [UIImage imageNamed:imageArr[i]];
    [topView addSubview:shaoView];
    
    UILabel *shaoLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2, 42,SCREEN_WIDTH/2-80, 16)];
    shaoLabel.font = [UIFont systemFontOfSize:14];
    shaoLabel.text = titleArr[i];
    shaoLabel.textColor = [UIColor whiteColor];
    shaoLabel.textAlignment = NSTextAlignmentRight;
    [topView addSubview:shaoLabel];
        
    }
    
    [self getDeiceData];
        
}
-(void)getDeiceData
{
    
    [[WangZhangNetWork sharedNetManager]FindFamilyByParams:nil withConpletionBlock:^(id responseObj, NSError *error) {
        
        NSLog(@"%@==%@",responseObj,error);
        
        
        YoShengHuoHomeData *list = [YoShengHuoHomeData yy_modelWithJSON:responseObj];
        
        [YoShengHuoHomeData setInstance:list];
        
        self.curFamily = [YoShengHuoHomeData shareInstance].data[0];
        
        
        
        NSLog(@"%@",self.curFamily.homeId);
        
    }];
    
    
}
-(void)singleSelected:(UIButton *)sender
{
    if (sender.tag ==1) {
        
        ShebeiViC *shebei = [[ShebeiViC alloc]init];
        
        [self.navigationController pushViewController:shebei animated:YES];
        
    }else if (sender.tag ==2){
        
        ChangjingViC *changjing = [[ChangjingViC alloc]init];
        
        [self.navigationController pushViewController:changjing animated:YES];
        
    }else if (sender.tag ==3){
        
        GuanLiViC *guanli = [[GuanLiViC alloc]init];
        
        [self.navigationController pushViewController:guanli animated:YES];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
