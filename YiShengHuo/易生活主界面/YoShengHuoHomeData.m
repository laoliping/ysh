//
//  YoShengHuoHomeData.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/30.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "YoShengHuoHomeData.h"

@implementation YoShengHuoHomeData

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{ @"data" : [YoShengHuoHomeDataInfo class]
              };
}
+ (YoShengHuoHomeData *)shareInstance
{
    static dispatch_once_t once;
    static YoShengHuoHomeData *instance = nil;
    dispatch_once(&once, ^{
        instance = [[YoShengHuoHomeData alloc]init];
    });
    return instance;
}

+(void)setInstance:(YoShengHuoHomeData *)info{
    
    [YoShengHuoHomeData shareInstance].data = info.data;
}
@end

@implementation YoShengHuoHomeDataInfo

@end
