//
//  ChangjingCell.h
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChangjingCell : UITableViewCell

@property (nonatomic, strong) UIImageView *nameImg;
@property (nonatomic, strong) UILabel *nameLabel; 


@end

NS_ASSUME_NONNULL_END
