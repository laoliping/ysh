//
//  ChangjingModel.h
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChangjingModel : NSObject

@property (nonatomic,strong)NSArray *data;

@end


@interface ChangjingModelItem : NSObject

@property (nonatomic,strong)NSString *sceneId;
@property (nonatomic,copy)NSString *sceneName;
@property (nonatomic,copy)NSString *homeId;
@property (nonatomic,copy)NSString *iconPath;
@property (nonatomic,copy)NSString *sceneType;
@property (nonatomic,copy)NSString *creatorId;
@property (nonatomic,copy)NSString *sceneStatus;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *createTime;
@property (nonatomic,copy)NSString *updateTime;
@property (nonatomic,copy)NSString *createBy;
@property (nonatomic,copy)NSString *updateBy;


@end

NS_ASSUME_NONNULL_END
