//
//  ChangjingModel.m
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "ChangjingModel.h"

@implementation ChangjingModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{ @"data" : [ChangjingModelItem class]
              };
}

@end

@implementation ChangjingModelItem

@end
