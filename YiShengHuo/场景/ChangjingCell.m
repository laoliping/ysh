//
//  ChangjingCell.m
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "ChangjingCell.h"

@implementation ChangjingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.nameImg = [[UIImageView alloc] initWithFrame:CGRectMake(14, 5, (SCREEN_WIDTH-28), 168)];
        self.nameImg.alpha = 0.7;
        self.nameImg.layer.cornerRadius =10;
        self.nameImg.layer.masksToBounds = YES;
        [self.contentView addSubview:self.nameImg];
        
        self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(34,14+5, SCREEN_WIDTH-98, 30)];;
        self.nameLabel.font = [UIFont systemFontOfSize:18];
        self.nameLabel.textAlignment = NSTextAlignmentLeft;
        self.nameLabel.textColor = [UIColor whiteColor];
        [self.contentView addSubview:self.nameLabel];
       
        
    }
    return self;
}

@end
