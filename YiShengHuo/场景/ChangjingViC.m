//
//  ChangjingViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "ChangjingViC.h"

@interface ChangjingViC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *ChangjingTabView;
@property (nonatomic,strong)YoShengHuoHomeDataInfo *curFamily;
@property (nonatomic,strong)NSMutableArray *ChangjingData;

@end

@implementation ChangjingViC
-(instancetype)init
{
    if (self=[super init]) {
        self.ChangjingData = [[NSMutableArray alloc]init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"场景";
    
     [self getChangJingList];
        
    self.ChangjingTabView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) style:UITableViewStylePlain];
    self.ChangjingTabView.dataSource = self;
    self.ChangjingTabView.delegate = self;
    [self.ChangjingTabView registerClass:[ChangjingCell class] forCellReuseIdentifier:@"ChangjingCell"];
    [self.view addSubview:self.ChangjingTabView];
    
    self.ChangjingTabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.ChangjingTabView.mj_header endRefreshing];
        [self getChangJingList];
        
        [self.ChangjingTabView.mj_footer resetNoMoreData];
    }];
    
}

-(void)getChangJingList {

    
    self.curFamily = [YoShengHuoHomeData shareInstance].data[0];
    
    [[WangZhangNetWork sharedNetManager]getSceneFindByHomeId:self.curFamily.homeId AndSceneType:@"1" withConpletionBlock:^(id responseObj, NSError *error) {
        
        NSLog(@"%@",responseObj);
              
        [self.ChangjingData removeAllObjects];
        
        ChangjingModel *list = [ChangjingModel yy_modelWithJSON:responseObj];
        NSLog(@"%@",list);
        
        [self.ChangjingData addObjectsFromArray:list.data];
        
        [self.ChangjingTabView reloadData];
        
        
    }];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ChangjingData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 178;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChangjingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChangjingCell" forIndexPath:indexPath];
    
    ChangjingModelItem *model = [self.ChangjingData objectAtIndex:indexPath.row];
    
   [cell.nameImg sd_setImageWithURL:[NSURL URLWithString:model.iconPath] completed:nil];
    cell.nameLabel.text = model.sceneName;
   
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ChangjingModelItem *model = [[ChangjingModelItem alloc]init];
    model= self.ChangjingData[indexPath.row];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText=[NSString stringWithFormat:@"正在执行%@",model.sceneName];
    [self.view addSubview:hud];
    
    [[WangZhangNetWork sharedNetManager]zhixingSceneStartBySceneId:[NSString stringWithFormat:@"%@",model.sceneId] withConpletionBlock:^(id responseObj, NSError *error) {
        
        [hud hide:YES afterDelay:1.0];
        
    }];
}


@end
