//
//  WebCollectionController.m
//  YiShengHuo
//
//  Created by macbook on 2020/9/23.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "WebCollectionController.h"
#import <WebKit/WebKit.h>

@interface WebCollectionController ()<WKUIDelegate,WKNavigationDelegate>

@property (nonatomic,strong)MBProgressHUD*hud;

@end

@implementation WebCollectionController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    NSLog(@"网址网址：：：%@",self.jutiURL);
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, -20, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:webView];
    webView.UIDelegate = self;
    webView.navigationDelegate = self;
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.jutiURL]]];
}
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [self.hud hide:YES afterDelay:1.0];
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
     [self.hud hide:YES afterDelay:1.0];
}

@end
