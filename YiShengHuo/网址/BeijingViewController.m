//
//  BeijingViewController.m
//  YiShengHuo
//
//  Created by macbook on 2020/9/23.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "BeijingViewController.h"
#import <AVOSCloud/AVOSCloud.h>
#import "WebCollectionController.h"

@interface BeijingViewController ()

@end

@implementation BeijingViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *imageView =  [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2 - 67.5, SCREEN_HEIGHT/2-67.5,135 , 135)];
    UIImage *image = [UIImage imageNamed:@"mainIcon"];
    imageView.image = image;
    [self.view addSubview:imageView];
    
    AVQuery *query = [AVQuery queryWithClassName:@"chaxunData"];
       
   [query getObjectInBackgroundWithId:@"5f6b1c050ed95f044fa9dd10" block:^(AVObject * _Nullable object, NSError * _Nullable error) {
       NSLog(@"%@==%@",object,error);
       
       NSString *typeStr = [NSString stringWithFormat:@"%@",object[@"type"]];
       if ([typeStr isEqualToString:@"0"]) {
           WebCollectionController *web = [[WebCollectionController alloc]init];
           UINavigationController *webNac = [[UINavigationController alloc]initWithRootViewController:web];
           web.jutiURL = [NSString stringWithFormat:@"%@",object[@"dizhiurl"]];
           self.view.window.rootViewController = webNac;
       }else{
           DengluViC *dengluVC = [DengluViC new];
           UINavigationController *dengluNac = [[UINavigationController alloc]initWithRootViewController:dengluVC];
           self.view.window.rootViewController = dengluNac;
           
       }
       
       
   }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
