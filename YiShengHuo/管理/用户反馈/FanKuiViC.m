//
//  FanKuiViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "FanKuiViC.h"

@interface FanKuiViC ()<UITextViewDelegate,UITextFieldDelegate>
{
    UITextView *textView;
    UILabel *tishiLabel;
}

@end

@implementation FanKuiViC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"用户反馈";
    [self feedBackView];
    [self BarButtonItem];
}
-(void)BarButtonItem
{
    UIButton *fankuiBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    fankuiBtn.frame = CGRectMake(0, 0, 30, 15);
    [fankuiBtn setTitle:@"反馈" forState:UIControlStateNormal];
    fankuiBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    fankuiBtn.contentMode = UIViewContentModeScaleAspectFit;
    [fankuiBtn addTarget:self action:@selector(finishClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:fankuiBtn];
}
-(void)feedBackView
{
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 100, [[UIScreen mainScreen] bounds].size.width, 220)];
    topView.userInteractionEnabled = YES;
    [self.view addSubview:topView];
    
    textView = [[UITextView alloc]initWithFrame:CGRectMake(5, 10, [[UIScreen mainScreen] bounds].size.width -10, 180)];
    textView.delegate = self;
    textView.editable = YES;
    textView.font=[UIFont systemFontOfSize:15];
    textView.layer.borderWidth = 1.0;
    textView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [topView addSubview:textView];
    
    tishiLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, [[UIScreen mainScreen] bounds].size.width -30, 34)];
    tishiLabel.enabled = NO;
    tishiLabel.text = @"请告诉我，你遇到了什么问题，或者希望我们有啥改进，谢谢！";
    tishiLabel.font =  [UIFont systemFontOfSize:14];
    tishiLabel.textColor = [UIColor lightGrayColor];
    tishiLabel.numberOfLines = 0;
    [textView addSubview:tishiLabel];
    
    
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    [tishiLabel setHidden:YES];
    return YES;
}
#pragma LipingMark === 反馈
-(void)finishClicked:(UIButton *)sender
{
    [self textFieldHide];
    
    NSString *string = textView.text;
    
    if (string.length == 0) {
        [JRToast showWithText:@"请输入反馈内容" duration:3.0];
        return;
    }
    [JRToast showWithText:@"反馈成功" duration:3.0];
    [self.navigationController popViewControllerAnimated:YES];
   
}

-(void)textFieldHide{
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
