//
//  GuanLiViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/22.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "GuanLiViC.h"


@interface GuanLiViC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *MainSetTableView;

@end

@implementation GuanLiViC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = @"管理";
    
    self.MainSetTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) style:UITableViewStyleGrouped];
    self.MainSetTableView.dataSource = self;
    self.MainSetTableView.delegate = self;
    [self.view addSubview:self.MainSetTableView];
    
    self.MainSetTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.MainSetTableView.mj_header endRefreshing];
    
    }];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 1)];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 1)];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   NSArray *nameArr = @[@"查看设备",@"智能场景",@"消息中心",@"关于版本",@"用户反馈",@"清除缓存",@"修改密码",@"退出登录"];
   
   UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"LSQaboutCell"];
   if (cell==nil) {
       cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GuanliCell"];
   }
    
    cell.textLabel.text =  [nameArr objectAtIndex:indexPath.row];
    if (indexPath.row==7) {
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
     
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0) {

        YISheBeiViewController *shebei = [YISheBeiViewController new];
        
        [self.navigationController pushViewController:shebei animated:NO];
        
    }
    if(indexPath.row == 1) {

        ZhiNeListViC *zhineng = [[ZhiNeListViC alloc]init];
        
        [self.navigationController pushViewController:zhineng animated:NO];

    }
    if( indexPath.row == 2) {

        MessageCenterViC *xiaoxi = [MessageCenterViC new];
        [self.navigationController pushViewController:xiaoxi animated:NO];

    }
    if( indexPath.row == 3) {

        BanBenViC *banben = [BanBenViC new];
        [self.navigationController pushViewController:banben animated:NO];

    }
    if( indexPath.row == 4) {

        FanKuiViC *fankui = [FanKuiViC new];
        [self.navigationController pushViewController:fankui animated:NO];

    }
    if(indexPath.row == 5) {

        [self qingchuhuancun];
    }
    if(indexPath.row == 6) {
        XiuGaiViC *xiugai = [XiuGaiViC new];
        [self.navigationController pushViewController:xiugai animated:NO];

    }
    if(indexPath.row == 7) {
        
        DengluViC *loginVC = [DengluViC new];
        UINavigationController *loginNac = [[UINavigationController alloc]initWithRootViewController:loginVC];
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        delegate.window.rootViewController = loginNac;
        
    }
    
    
}
-(void)qingchuhuancun
{
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachePath];

    NSString *huancunStr = [NSString stringWithFormat:@"确认清除%lu个文件的缓存吗？",(unsigned long)[files count]];

    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"温馨提示"message:huancunStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* OK = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
        [alert dismissViewControllerAnimated: YES completion:nil];
        for (NSString *p in files){
        NSError *error;
        NSString *path = [cachePath stringByAppendingString:[NSString stringWithFormat:@"/%@",p]];
        if([[NSFileManager defaultManager] fileExistsAtPath:path]){
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        }
     }
        
    }];

    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
                                 
        [alert dismissViewControllerAnimated: YES completion: nil];

    }];


    [alert addAction: OK];
    [alert addAction: cancel];

    [self presentViewController: alert animated: YES completion: nil];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
