//
//  ZhiNeListViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "ZhiNeListViC.h"

@interface ZhiNeListViC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *ZhiNeListTabView;
@property (nonatomic,strong)YoShengHuoHomeDataInfo *curFamily;
@property (nonatomic,strong)NSMutableArray *ZhiNeData;


@end

@implementation ZhiNeListViC

-(instancetype)init
{
    if (self=[super init]) {
        self.ZhiNeData = [[NSMutableArray alloc]init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"智能场景";
    [self getZhiNengList];
    
    self.ZhiNeListTabView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) style:UITableViewStylePlain];
    self.ZhiNeListTabView.dataSource = self;
    self.ZhiNeListTabView.delegate = self;
    [self.view addSubview:self.ZhiNeListTabView];
    
    self.ZhiNeListTabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.ZhiNeListTabView.mj_header endRefreshing];
        [self getZhiNengList];
        
        [self.ZhiNeListTabView.mj_footer resetNoMoreData];
    }];
    
}

-(void)getZhiNengList {
    
    self.curFamily = [YoShengHuoHomeData shareInstance].data[0];
       
   [[WangZhangNetWork sharedNetManager]getSceneFindByHomeId:self.curFamily.homeId AndSceneType:@"1" withConpletionBlock:^(id responseObj, NSError *error) {
       
        NSLog(@"%@",responseObj);
              
        [self.ZhiNeData removeAllObjects];
       
       ChangjingModel *list = [ChangjingModel yy_modelWithJSON:responseObj];
       NSLog(@"%@",list);
       
       [self.ZhiNeData addObjectsFromArray:list.data];
       
       [self.ZhiNeListTabView reloadData];
       
       
   }];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ZhiNeData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"zhinengCell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"deviceCell"];
    }
    ChangjingModelItem *model = [self.ZhiNeData objectAtIndex:indexPath.row];
    cell.textLabel.text = model.sceneName;
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


@end
