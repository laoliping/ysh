//
//  XiuGaiViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "XiuGaiViC.h"

@interface XiuGaiViC ()<UITextFieldDelegate>
{
    UITextField *oldPwdTextField;
    UITextField *pwdTextField;
}

@end

@implementation XiuGaiViC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"修改密码";
    [self xiugaiView];
    
}
#pragma mark === 修改密码接口
-(void)finishClicked:(UIButton *)sender
{
    [self textFieldHide];
    if (!oldPwdTextField.text||[oldPwdTextField.text isEqualToString:@""]) {
        [JRToast showWithText:@"请输入内容" bottomOffset:100.f duration:3.0f];
        return;
    }
    if (!pwdTextField.text||[pwdTextField.text isEqualToString:@""]) {
        [JRToast showWithText:@"请输入内容" bottomOffset:100.f duration:3.0f];
        return;
    }
    if (pwdTextField.text.length < 6) {
        [JRToast showWithText:@"密码不能少于6位" bottomOffset:100.f duration:3.0f];
        return;
    }
    NSDictionary *param = @{@"password":oldPwdTextField.text,@"newPassword":pwdTextField.text};
    
    [[WangZhangNetWork sharedNetManager]ModifyPwdByParams:param withConpletionBlock:^(id responseObj, NSError *error) {
       
        if ([responseObj[@"code"] integerValue] == 0) {
            
            DengluViC *loginVC = [DengluViC new];
            UINavigationController *loginNac = [[UINavigationController alloc]initWithRootViewController:loginVC];
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            delegate.window.rootViewController = loginNac;
            
        }else{
            [JRToast showWithText:[NSString stringWithFormat:@"%@",responseObj[@"msg"]] bottomOffset:100.0f duration:3.0f];
            return ;
        }
    }];
    
}
-(void)xiugaiView
{
    
    oldPwdTextField = [[UITextField alloc] initWithFrame:CGRectMake(47, 267, SCREEN_WIDTH- 47*2, 42)];
    oldPwdTextField.borderStyle = UITextBorderStyleRoundedRect;
    oldPwdTextField.adjustsFontSizeToFitWidth = YES;
    oldPwdTextField.placeholder = @"请输入旧密码";
    oldPwdTextField.clearsOnBeginEditing = YES;
    oldPwdTextField.delegate = self;
    oldPwdTextField.tag =56;
    oldPwdTextField.secureTextEntry = YES;
    oldPwdTextField.leftViewMode = UITextFieldViewModeAlways;
    oldPwdTextField .keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:oldPwdTextField];

        
    pwdTextField = [[UITextField alloc] initWithFrame:CGRectMake(47, 339, SCREEN_WIDTH- 47*2, 42)];
    pwdTextField.borderStyle = UITextBorderStyleRoundedRect;
    pwdTextField.placeholder = @"请输入新密码";
    pwdTextField.clearsOnBeginEditing = YES;
    pwdTextField.delegate = self;
    pwdTextField.tag =78;
    pwdTextField.secureTextEntry = YES;
    pwdTextField.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:pwdTextField];

    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    loginBtn.frame = CGRectMake(47 , 411, SCREEN_WIDTH-94, 48);
    [loginBtn setTitle:@"确定" forState:UIControlStateNormal];
    loginBtn.tintColor = [UIColor whiteColor];
    [loginBtn addTarget:self action:@selector(finishClicked:) forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = MainColor;
    [loginBtn.layer setMasksToBounds:YES];
    [loginBtn.layer setCornerRadius:6.0];
    [self.view addSubview:loginBtn];
   
    
}

#pragma mark === 限制输入长度
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField.tag==56||textField.tag ==78) {
        if (string.length == 0)
            return YES;
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        
        if (existedLength - selectedLength + replaceLength > 16) {
            return NO;
        }
    }
    return YES;
}
//键盘消失
-(void)textFieldHide{
    [self.view endEditing:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
