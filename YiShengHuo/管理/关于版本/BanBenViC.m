//
//  BanBenViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "BanBenViC.h"

@interface BanBenViC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *BanBenTabView;


@end

@implementation BanBenViC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"关于版本";
    
    self.BanBenTabView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) style:UITableViewStylePlain];
   self.BanBenTabView.dataSource = self;
   self.BanBenTabView.delegate = self;
   [self.view addSubview:self.BanBenTabView];
   
   self.BanBenTabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
       [self.BanBenTabView.mj_header endRefreshing];
      
       
       [self.BanBenTabView.mj_footer resetNoMoreData];
   }];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"aboutCell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"deviceCell"];
    }
    cell.textLabel.text = @"当前版本";
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [JRToast showWithText:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] duration:3.0];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
