//
//  MessageModel.m
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "MessageModel.h"

@implementation MessageModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"data" : [MessageModelItem class]
            };
}

@end

@implementation MessageModelItem

@end
