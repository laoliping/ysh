//
//  MessageModel.h
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageModel : NSObject

@property (nonatomic,strong)NSArray *data;

@end

@interface MessageModelItem : NSObject


@property (nonatomic,copy)NSString *deviceId;
@property (nonatomic,copy)NSString *noticeStatus;
@property (nonatomic,copy)NSString *deviceStatus;
@property (nonatomic,copy)NSString *userId;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *msgType;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *createTime;
@property (nonatomic,copy)NSNumber *updateTime;

@end

NS_ASSUME_NONNULL_END
