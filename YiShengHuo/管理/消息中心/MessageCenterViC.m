//
//  MessageCenterViC.m
//  YiShengHuo
//
//  Created by honfan on 2020/5/9.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "MessageCenterViC.h"

@interface MessageCenterViC ()<UITableViewDataSource,UITableViewDelegate>
{
    UILabel *NUllLabel;
}
@property (nonatomic, strong) UITableView *MessageCenterTableView;
@property (nonatomic,strong)NSMutableArray *MesgdataSource;

@end

@implementation MessageCenterViC

-(instancetype)init
{
    if (self=[super init]) {
        
        self.MesgdataSource = [[NSMutableArray alloc]init];
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息中心";
    
    [self getMessageData];
    
    self.MessageCenterTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,  [[UIScreen mainScreen] bounds].size.width,  [[UIScreen mainScreen] bounds].size.height) style:UITableViewStylePlain];
    self.MessageCenterTableView.dataSource = self;
    self.MessageCenterTableView.delegate = self;
    [self.view addSubview:self.MessageCenterTableView];
    
    self.MessageCenterTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.MessageCenterTableView.mj_header endRefreshing];
        [self getMessageData];
      
        [self.MessageCenterTableView.mj_footer resetNoMoreData];
    }];
}
-(void)getMessageData
{
     NSDictionary *dic = @{@"msgMode":@"1"};
    [[WangZhangNetWork sharedNetManager]GetMsgfindByParams:dic AndPageNum:@"1" withConpletionBlock:^(id responseObj, NSError *error) {
        
        [self.MesgdataSource removeAllObjects];
               
       MessageModel *list = [MessageModel yy_modelWithJSON:responseObj];
       
       [self.MesgdataSource addObjectsFromArray:list.data];
       if (self.MesgdataSource.count == 0) {
           
           if (self->NUllLabel == nil) {
               self->NUllLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,  [[UIScreen mainScreen] bounds].size.height/3,  [[UIScreen mainScreen] bounds].size.width, 14)];
           }
           self->NUllLabel.text = @"暂无消息数据";
           self->NUllLabel.textAlignment = NSTextAlignmentCenter;
           self->NUllLabel.textColor = [UIColor lightGrayColor];
           self->NUllLabel.font = [UIFont systemFontOfSize:14];
           [self.view addSubview:self->NUllLabel];
       }
       
       
       [self.MessageCenterTableView reloadData];
               
        
    }];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.MesgdataSource.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"zhinengCell"];
   if (cell==nil) {
       cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"deviceCell"];
   }
   MessageModelItem *model = [self.MesgdataSource objectAtIndex:indexPath.row];
   cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ %@",model.title,[self dateStringFrom:model.updateTime withFormat:@"yyyy-MM-dd HH:mm"],model.content];
   cell.textLabel.textColor = [UIColor blackColor];
   cell.textLabel.textAlignment = NSTextAlignmentLeft;
   cell.textLabel.font = [UIFont systemFontOfSize:14];
   cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
   return cell;
    
   
}
-(NSString *)dateStringFrom:(NSNumber *)date withFormat:(NSString *)format {
    NSDate *aDate = [NSDate dateWithTimeIntervalSince1970:[date doubleValue]/1000];
    NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
    fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    fmt.dateFormat = format;
    NSString* dateString = [fmt stringFromDate:aDate];
    return dateString;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
