//
//  DEviceModel.h
//  YiShengHuo
//
//  Created by honfan on 2020/4/30.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DEviceModel : NSObject

@property (nonatomic,strong)NSArray *data;

@end

@interface DEviceModelItem : NSObject

@property (nonatomic,strong)NSString *homeDeviceId;
@property (nonatomic,copy)NSString *deviceId;
@property (nonatomic,copy)NSString *deviceType;
@property (nonatomic,copy)NSString *deviceNick;
@property (nonatomic,copy)NSString *iconPath;
@property (nonatomic,copy)NSString *deviceStatus;
@property (nonatomic,copy)NSString *homeId;
@property (nonatomic,copy)NSString *ownerId;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,strong)NSArray *deviceEndpoints;

@end

NS_ASSUME_NONNULL_END
