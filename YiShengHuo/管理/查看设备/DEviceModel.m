//
//  DEviceModel.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/30.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "DEviceModel.h"

@implementation DEviceModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{ @"data" : [DEviceModelItem class]
              };
}
@end


@implementation DEviceModelItem

@end
