//
//  YISheBeiViewController.m
//  YiShengHuo
//
//  Created by honfan on 2020/4/30.
//  Copyright © 2020 WangZhangPersonal. All rights reserved.
//

#import "YISheBeiViewController.h"

@interface YISheBeiViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *shebeiTabView;
@property (nonatomic,strong)YoShengHuoHomeDataInfo *curFamily;
@property (nonatomic,strong)NSMutableArray *shebeiData;

@end

@implementation YISheBeiViewController

-(instancetype)init
{
    if (self=[super init]) {
        self.shebeiData = [[NSMutableArray alloc]init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"查看设备列表";
    [self getSimpleDeviceList];
    
    self.shebeiTabView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) style:UITableViewStylePlain];
    self.shebeiTabView.dataSource = self;
    self.shebeiTabView.delegate = self;
    [self.view addSubview:self.shebeiTabView];
    
    self.shebeiTabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.shebeiTabView.mj_header endRefreshing];
        [self getSimpleDeviceList];
        
        [self.shebeiTabView.mj_footer resetNoMoreData];
    }];
    
}

-(void)getSimpleDeviceList {
    
    self.curFamily = [YoShengHuoHomeData shareInstance].data[0];
    
    NSLog(@"%@",self.curFamily);
    
    NSDictionary *param = @{@"homeId":self.curFamily.homeId};

    
    [[WangZhangNetWork sharedNetManager]FindHomeDeviceByParams:param andPageNum:@"1" andPagesize:@"1000" withConpletionBlock:^(id responseObject, NSError *error) {
        
        NSLog(@"%@",responseObject);
        
        [self.shebeiData removeAllObjects];
        
        DEviceModel *list = [DEviceModel yy_modelWithJSON:responseObject];
        NSLog(@"%@",list);
        
        [self.shebeiData addObjectsFromArray:list.data];
        
        [self.shebeiTabView reloadData];
        
        
        
    }];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shebeiData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"deviceCell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"deviceCell"];
    }
    DEviceModelItem *model = [self.shebeiData objectAtIndex:indexPath.row];
    cell.textLabel.text = model.deviceNick;
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


@end
