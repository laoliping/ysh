//
//  UIViewController+HLF.h
//  GeiNiHua
//
//  Created by ChenYu on 17/3/24.
//  Copyright © 2017年 GNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (HLF)

// 根控制器present出来的最上面的viewController
+ (UIViewController *)hlf_stackTopViewController;

// 当前控制器present出来的最上面的viewcontroller
- (UIViewController *)hlf_visibleViewController;

// 最下面的presentingViewController
- (UIViewController *)hlf_farthestPresentingViewController;

// 最顶层的Viewcontroller 不包括childViewController
+ (UIViewController *)hlf_toppestViewController;

// 获取rootVC开始的最上层viewController
+ (UIViewController *)hlf_toppestViewControllerFromRootVC:(UIViewController *)rootVC;

@end
