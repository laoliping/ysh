//
//  HLFConfigManager.h
//  Test
//
//  Created by ChenYuan on 2020/9/18.
//  Copyright © 2020 ChenYuan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HLFConfigManager : NSObject

+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
