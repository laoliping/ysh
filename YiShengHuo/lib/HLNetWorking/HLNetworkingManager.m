//
//  HLNetworkingManager.m
//  HaiLang
//
//  Created by ChenYuan on 2020/9/18.
//  Copyright © 2020 ChenYuan. All rights reserved.
//

#import "HLNetworkingManager.h"

static NSTimeInterval const MDFRequestTimeoutForWifi = 30;
static NSTimeInterval const MDFRequestTimeoutForOther = 45;

@implementation HLNetworkingManager

+ (instancetype)sharedInstance
{
    static HLNetworkingManager *networkManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        networkManager = [[HLNetworkingManager alloc] init];
    });
    
    return networkManager;
}

+ (AFHTTPSessionManager *)sessionManagerForBaseURL:(NSString *)baseUrl httpHeader:(nullable NSDictionary *)httpHeader
{
    if (!baseUrl.length) {
        return NULL;
    }

    AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
    sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    sessionManager.securityPolicy = [AFSecurityPolicy defaultPolicy];

    // 请求的超时时间
    if (sessionManager.reachabilityManager.reachableViaWiFi) {
        sessionManager.requestSerializer.timeoutInterval = MDFRequestTimeoutForWifi;
    } else {
        sessionManager.requestSerializer.timeoutInterval = MDFRequestTimeoutForOther;
    }
    
    // 设置请求头参数
    [sessionManager.requestSerializer setValue:httpHeader[@"xxx_alise"] forHTTPHeaderField:@"User-Agent"];
    [sessionManager.requestSerializer setValue:httpHeader[@"Authorization"] forHTTPHeaderField:@"Authorization"];

    // 请求的超时时间
    if (sessionManager.reachabilityManager.reachableViaWiFi) {
        sessionManager.requestSerializer.timeoutInterval = MDFRequestTimeoutForWifi;
    } else {
        sessionManager.requestSerializer.timeoutInterval = MDFRequestTimeoutForOther;
    }
    sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"image/png",@"utf-8", @"text/json", @"text/javascript" ,nil];
    
    return sessionManager;
}

#pragma mark - Class Method

- (void)requestBaseUrl:(NSString *)baseUrl withAddress:(NSString *)UrlAddress params:(NSDictionary *)params requesType:(HLFRequestType)requestType completeBlock:(void (^)(id _Nullable, BOOL))completeBlock
{
    AFHTTPSessionManager *sessionManager = [HLNetworkingManager sessionManagerForBaseURL:baseUrl httpHeader:self.httpHeader];
    NSDictionary *allParam = [NSDictionary dictionaryWithDictionary:params];
    __weak typeof(self) weakSelf = self;
    switch (requestType) {
        case HLFRequestTypePost: {
            [sessionManager POST:UrlAddress
                      parameters:allParam
                         headers:nil
                        progress:nil
                         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    [strongSelf successBlockCall:responseObject completeBlock:completeBlock];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    [strongSelf failureBlockCall:error completeBlock:completeBlock];
                }
            }];
            break;
        }
        case HLFRequestTypeGet: {
            [sessionManager GET:UrlAddress parameters:allParam headers:nil progress:^(NSProgress * _Nonnull downloadProgress) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    if (weakSelf.downloadPercentBlock) {
                        strongSelf.downloadProgress = downloadProgress.fractionCompleted;
                        weakSelf.downloadPercentBlock(strongSelf.downloadProgress);
                    }
                }
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    [strongSelf successBlockCall:responseObject completeBlock:completeBlock];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    [strongSelf failureBlockCall:error completeBlock:completeBlock];
                }
            }];
            break;
        }
        case HLFRequestTypeHead: {
            [sessionManager HEAD:UrlAddress parameters:allParam headers:nil success:^(NSURLSessionDataTask * _Nonnull task) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    [strongSelf successBlockCall:nil completeBlock:completeBlock];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    [strongSelf failureBlockCall:error completeBlock:completeBlock];
                }
            }];
            break;
        }
    }
}

- (void)successBlockCall:(id)responseObject completeBlock:(void (^)(id  _Nullable, BOOL))completeBlock
{
    if (completeBlock) {
        completeBlock(responseObject, YES);
    }
}

- (void)failureBlockCall:(NSError *)error completeBlock:(void (^)(id  _Nullable, BOOL))completeBlock
{
    if (completeBlock) {
        completeBlock(error, NO);
    }
}


@end
