//
//  AQUpgradeManager.m
//  AiQiu
//
//  Created by ChenYuan on 2020/9/13.
//  Copyright © 2020 lesports. All rights reserved.
//

#import "AQUpgradeManager.h"
#import "AQUpgradeView.h"
#import "HLNetworkingManager.h"
#import "HLFKit.h"

typedef NS_ENUM(NSInteger, HLFUpgradeType) {
    HLFUpgradeTypeForce = 0,  // 强制升级
    HLFUpgradeTypeRemind,     // 提示升级
    HLFUpgradeTypeNotUpgrade, // 不升级
};

@interface AQUpgradeManager ()

@property (nonatomic, copy) NSString *version;

@property (nonatomic, weak) AQUpgradeView *updateBgView;

@end

@implementation AQUpgradeManager

+ (void)load
{
    [super load];

    // Config
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[AQUpgradeManager sharedInstance] upgradeManager];
    });
}


#pragma mark - LifeCycle

+ (instancetype)sharedInstance
{
    static AQUpgradeManager *upgradeManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        upgradeManager = [[AQUpgradeManager alloc] init];
    });
    
    return upgradeManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupDatas];
    }
    return self;
}

#pragma mark - SetupData

- (void)setupDatas
{
    NSDictionary *dict = [[NSBundle mainBundle] infoDictionary];
    self.version = dict[@"CFBundleShortVersionString"];
}

- (void)upgradeManager
{
    // 请求
    __weak typeof(self) weakSelf = self;
    
    NSMutableDictionary *parama = [NSMutableDictionary dictionary];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    NSArray <NSString *>*bundles = [bundleIdentifier componentsSeparatedByString:@"."];
     NSString *appSign = bundleIdentifier;
     if (bundles.count == 3) {
         appSign = bundles[1];
     }
    parama[@"appSign"] = appSign;
    parama[@"platform"] = @"iOS";

    [[HLNetworkingManager sharedInstance] requestBaseUrl:kBaseUrl
                                             withAddress:kUpgradeAddress
                                                  params:parama
                                              requesType:HLFRequestTypeGet
                                           completeBlock:^(id responseObject , BOOL isSuccess) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSString *versionCode = responseObject[@"versionCode"]; // 版本号
            // 升级类型: 0:强制 1:提示 2:不升级 ,
            HLFUpgradeType upgradeType = [responseObject[@"upgradeType"] integerValue];
            NSString *content = responseObject[@"content"];
            NSString *downloadUrl = responseObject[@"downloadUrl"];  // 跳转url
            if (isSuccess) {
                if (!weakSelf.updateBgView) {
                    if ([self.version compare:versionCode options:NSNumericSearch] == NSOrderedAscending) {
                        // 小于 version 的版本
                        if (upgradeType == HLFUpgradeTypeForce) {
                            // 强制升级
                            self.updateBgView = [AQUpgradeView setupView:content upgradeUrl:downloadUrl  version:versionCode isForceUpgrade:YES];
                        } else if (upgradeType == HLFUpgradeTypeRemind) {
                            // 普通升级
                            self.updateBgView = [AQUpgradeView setupView:content upgradeUrl:downloadUrl  version:versionCode isForceUpgrade:NO];
                        }
                    }
                }
            }
        }
    }];
}

@end
