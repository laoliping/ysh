//
//  AQUpgradeManager.h
//  AiQiu
//
//  Created by ChenYuan on 2020/9/13.
//  Copyright © 2020 lesports. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AQUpgradeManager : NSObject

+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
