//
//  HLFUmengConfig.m
//  Test
//
//  Created by ChenYuan on 2020/9/21.
//  Copyright © 2020 ChenYuan. All rights reserved.
//

#import "HLFUmengConfig.h"
#import <UMAnalytics/MobClick.h>
#import <UMCommon/UMCommon.h>
#import "HLFKit.h"

@implementation HLFUmengConfig

+ (void)load
{
    [super load];

    // Config
    [HLFUmengConfig sharedInstance];
}

#pragma mark - LifeCycle

+ (instancetype)sharedInstance
{
    static HLFUmengConfig *configManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        configManager = [[HLFUmengConfig alloc] init];
    });
    
    return configManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self umAnalyticsConfig];
    }
    return self;
}

- (void)umAnalyticsConfig
{
    [UMConfigure initWithAppkey:accesskey channel:@"AppStore"];
}

@end
