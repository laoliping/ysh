//
//  DDPlayGameViewController.h
//  Test
//
//  Created by ChenYuan on 2020/9/10.
//  Copyright © 2020 ChenYuan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DDPlayGameViewController : UIViewController

- (void)configRedirectUrl:(NSString *)redirectUrl;

@end

NS_ASSUME_NONNULL_END
