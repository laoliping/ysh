//
//  DDMainViewController.h
//  Test
//
//  Created by ChenYuan on 2020/9/9.
//  Copyright © 2020 ChenYuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDMainViewController : UIViewController

@property (nonatomic, copy) NSString *redirectUrl; 

@end

